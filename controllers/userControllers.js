
/*
    Naming convention for controllerse is that it should be named after the model/documents it is concerned with
*/

// To create a controller, we first add it into our modules.exports
// So that we can import the controllers from our module

// import the User model in the controllers because this is where we're going to use it
const bcrypt = require("bcrypt");
const User = require("../models/User");
const auth = require("../auth")
const Course = require("../models/Course")

module.exports.registerUser = (req,res)=>{

    // console.log(req.body)

    // using bcrypt, we're going to hide the user's password underneath a layer of randomized characters. Salt rounds are the number of times we randomize the string/password hash

    const hashedPw = bcrypt.hashSync(req.body.password,10);

    console.log(hashedPw);

    let newUser = new User ({

        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPw,
        mobileNo: req.body.mobileNo
    })

    newUser.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

module.exports.getUserDetails = (req,res)=>{
    // console.log(req.user)
	User.findById({_id:req.user.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.loginUser = (req,res)=>{
   User.findOne({email:req.body.email})
   .then(foundUser =>{
    if(foundUser === null){
        return res.send({message: "No User Found."})
    } else {
        // console.log(foundUser)
        // check if the input pasword from req.body matches the hashed password in our foundUser document
        const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
        // console.log(isPasswordCorrect);

        if(isPasswordCorrect){
            // console.log("We will create a token for the user if the password is correct")
            return res.send({accessToken: auth.createAccessToken(foundUser)});
        } else {
            return res.send({message: "Incorrect Password"});
        }
    }
   })
}

// checks if Email exists or not
module.exports.checkEmail = (req,res)=>{
    User.findOne({email: req.body.email})
    .then(result => {
        if(result === null){
            return res.send(false)
        } else {
            return res.send(true)
        }
    })
    .catch(error => res.send(error))
}

module.exports.enroll = async (req,res) => {
    // console.log(req.user.id)
    // console.log(req.body.courseId)

    if(req.user.isAdmin){
        return res.send({message: "Action Forbidden"})
    }

    let isUserUpdated = await User.findById(req.user.id).then(user => {
        // console.log(user)
        let newEnrollement = {
            courseId: req.body.courseId
        }
        user.enrollment.push(newEnrollement)
        return user.save()
        .then(user => true)
        .catch(err => err.message)
    })
    // console.log(isUserUpdated);

    if(isUserUpdated !== true){
        return res.send({message:isUserUpdated})
    }

    let isCourseUpdated = await Course.findById(req.body.courseId).then(course=> {

        let enrollee = {
            userId: req.user.id
        }

        course.enrollees.push(enrollee);

        return course.save().then(course => true).catch(err => err.message);

    })
    // console.log(isCourseUpdated);
    if(isCourseUpdated !== true){
        return res.send({message:isCourseUpdated})
    }

    if(isUserUpdated && isCourseUpdated){
        return res.send({message:"Thank you for enrolling!"})
    }
}

