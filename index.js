const express = require("express");
// Monggose is an ODM library to let our expressjs api manipulate a mongoDB database
const mongoose = require("mongoose");
const app = express();
const port = 4000;

/*
	Mongoose connection

	mongoose.connect() is a method to connect our api with our mongodb database via the use of mongoose. It has 2 arguments. First, is the connection string to connect our api to our mongodb. second, is an object used to add information between mongoose and mongodb

*/

mongoose.connect("mongodb+srv://admin:admin123@cluster0.pnw34.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});
// We will create notification if the connections to the db is a success or failed. 
let db = mongoose.connection;
db.on('error',console.error.bind(console, "MongoDB Connection Error"));
db.once('open',()=>console.log("Connected to MongoDB"));

app.use(express.json());

// import our routes and use it as middleware.
// which means, that we will be able to group together our routes
const courseRoutes = require('./routes/courseRoutes');
// use our routes and group them together under '/courses'
// our endpoints are now prefaced with '/courses'
app.use('/courses',courseRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);


app.listen(port,() => console.log(`Express API running at port 4000`))
