const mongoose = require("mongoose");

/*
	Mongoose Schema
	Before we can create documents from our api to save into our database, we must first determine the dtructure of the doucments to be written in the database.

	Schema acts as a blueprint for our data/documnt

	A schema is a representation of how the doucment is structured. It also determines the types of data and the expected properties. 

	So, with this, we don't have to worry for if we had input "stock" in our documents, because we can make it uniform with a schema. 
*/

const courseSchema = new mongoose.Schema({
	name: {
		type:String,
		required:[true,"Name is Required"]
	},
	description: {
		type:String,
		required:[true,"Description is Required"]
	},
	price: {
		type:Number,
		required:[true,"Price is Required"]
	},
	isActive: {
		type:Boolean,
		default: true
	},
	createdOn: {
		type:Date,
		default: new Date()
	},
	enrollees: [
		{
			userId:{
				type:String,
				required:[true,"User Id is Required"]
			}, 
			dateEnrolled: {
				type:Date,
				default: new Date()
			},
			status: {
				type:String,
				default:"Enrolled"
			}
		}
	]

})

// module.experts - so we can import and use this file in another file. 
module.exports = mongoose.model("Course",courseSchema);

