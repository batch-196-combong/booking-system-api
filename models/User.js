const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type:String,
		required:[true,"Firts Name is Required"]
	},
	lastName: {
		type:String,
		required:[true,"lastName is Required"]
	},
	email: {
		type:String,
		required:[true,"Email is Required"]
	},
	password: {
		type:String,
		required:[true,"Password is Required"]
	},
	mobileNo: {
		type:String,
		required:[true,"Mobile Number is Required"]
	},
	isAdmin: {
		type:Boolean,
		default: false
	},
	createdOn: {
		type:Date,
		default: new Date()
	},
	enrollment: [
		{
			courseId:{
				type:String,
				required:[true,"Course Id is Required"]
			}, 
			dateEnrolled: {
				type:Date,
				default: new Date()
			},
			status: {
				type:String,
				default:"Enrolled"
			}
		}
	]

})

module.exports = mongoose.model("User",userSchema);
