const jwt = require ("jsonwebtoken");

const secret = "courseBookingAPI"

// JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow access certains of our application.

module.exports.createAccessToken = (userDetails) => {

	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}
	console.log(data);
	return jwt.sign(data,secret,{});
}

module.exports.verify = (req,res,next)=>{
	let token = req.headers.authorization 

	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token"});
	} else {

		token = token.slice(7);
		// console.log(token)

		jwt.verify(token,secret,function(err,decodedToken){
			// console.log(decodedToken);
			// console.log(err);

		if (err){
			return res.send({
				auth:"failed",
				message: err.message
			})
		}else {
			req.user = decodedToken
			next();
		}
		})
	}
}

module.exports.verifyAdmin = (req,res,next) => {
	if(req.user.isAdmin){
		next();
	} else {
		return res.send ({
			auth:"Failed",
			message: "Action Forbidden"
		})
	}

	// console.log(req.user);
}
