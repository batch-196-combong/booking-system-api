const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers")

const auth = require("../auth");

const{verify} = auth;

// register
router.post("/",userControllers.registerUser)

router.post('/details',verify,userControllers.getUserDetails);

router.post('/login',userControllers.loginUser);

router.post('/checkEmail',userControllers.checkEmail)

router.post('/enroll', verify, userControllers.enroll)

module.exports = router;